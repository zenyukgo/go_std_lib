package main

import (
	"strconv"
	"time"
)

func main() {
	var x = 10
	go adjustX(&x)

	// for with a single boolean
	for x > 0 {
		time.Sleep(time.Millisecond * 100)
		println("sleep " + strconv.Itoa(x))
	}
	println("exit")
}

func adjustX(x *int) {
	for {
		*x--
		time.Sleep(time.Second)
		println("x--" + strconv.Itoa(*x))
	}
}
