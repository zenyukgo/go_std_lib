custome pool size
==
// MaxIdleConns is the http conn. pool size
// default is 100
fmt.Printf("transport idle conn.: %+v\n", http.DefaultTransport.(*http.Transport).MaxIdleConns)

// default is 0, meaining no limit 
fmt.Printf("transport conn. per host: %+v\n", http.DefaultTransport.(*http.Transport).MaxConnsPerHost)
fmt.Printf("transport idle conn. per host: %+v\n", http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost)

// default is 90 sec.
fmt.Printf("transport idle conn. timeout: %+v\n", http.DefaultTransport.(*http.Transport).IdleConnTimeout)

// how to change
t := http.DefaultTransport.(*http.Transport).Clone()
t.MaxIdleConns = 1000

httpClient := &http.Client{
	Timeout:   90 * time.Second,
	Transport: t,
}
httpClient.Post()


cache DNS lookups
==
import "github.com/rs/dnscache"
...
// httpClient creates HTTP client with a custom connection pool size and DNS lookup caching
func httpClient(httpConPool int) *http.Client {
	r := &dnscache.Resolver{}
	t := http.DefaultTransport.(*http.Transport).Clone()
	// use custom HTTP connection pool size
	t.MaxIdleConns = httpConPool
	// use DNS lookup caching
	t.DialContext = func(ctx context.Context, network string, addr string) (conn net.Conn, err error) {
		host, port, err := net.SplitHostPort(addr)
		if err != nil {
			return nil, err
		}
		ips, err := r.LookupHost(ctx, host)
		if err != nil {
			return nil, err
		}
		for _, ip := range ips {
			var dialer net.Dialer
			conn, err = dialer.DialContext(ctx, network, net.JoinHostPort(ip, port))
			if err == nil {
				break
			}
		}
		return
	}

	return &http.Client{
		Timeout:   90 * time.Second,
		Transport: t,
	}
}
